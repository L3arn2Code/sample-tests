from pages.train import TrainPage
from datetime import datetime

import pytest

in_a_week = datetime.now().day + 7

def test_load_train_page(browser):
    # EXERCISE
    ## When the user loads the Main/Train page
    train_page = TrainPage(browser)
    train_page.load()
    search_title = train_page.get_main_search_title()
    # VERIFY
    ## Then the user sees the 'Search for railway tickets' text in the main search section
    assert 'Search for railway tickets' in search_title
    ## Then the user sees 2 logo images on the page
    assert train_page.count_logos() == 2
    ## Then the user sees 8 main sections on the page
    assert train_page.count_main_sections() == 8

@pytest.mark.parametrize('from_, to_, day', [('Poltava Kyivska', 'Kyiv-Pasazhyrsky', in_a_week)])
def test_route_search(browser, from_, to_, day):
    # EXERCISE
    ## When the user searches for the route
    train_page = TrainPage(browser)
    train_page.load()
    train_page.route(from_, to_, day)
    trips = train_page.trips()
    trips_number = train_page.trips_number()

    # VERIFY
    ## Then the departure, arrival stations and day from the result trips are equal to the input data
    assert all(from_ == trip['departure_station'] for trip in trips)
    assert all(to_ == trip['arrival_station'] for trip in trips)
    assert all(day == trip['departure time'].day for trip in trips)
    ## Then the quantity of result trips are equivalent to the trip-counter number
    assert len(trips) == trips_number

@pytest.mark.parametrize('option', ['departure time', 'arrival time', 'travel time'])
def test_sort_by(browser, option):
    # EXERCISE
    ## When the user sorts the result trips
    train_page = TrainPage(browser)
    train_page.load()
    train_page.route('Poltava Kyivska', 'Kyiv-Pasazhyrsky', in_a_week)
    if option != 'departure time':
        train_page.sort_by(option)
    trips = train_page.trips()

    # VERIFY
    ## Then the result trips are sorted
    assert all(trips[i][option] <= trips[i+1][option] for i in range(len(trips)-1))
